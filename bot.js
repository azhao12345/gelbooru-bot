var TelegramBot = require('node-telegram-bot-api');
var https = require('https');
var url = require('url');
var querystring = require('querystring');
var parseString = require('xml2js').parseString;

var token = '';
var bot = new TelegramBot(token, {polling: {interval: 50}});


bot.on('inline_query', function(message) { 
    console.log(message);
    var a = {
        type: 'photo',
        id: '12345',
        photo_url: 'https://assets.gelbooru.com/thumbnails/c0/68/thumbnail_c06832c93190064f5da28ee692811f4f.jpg',
        thumb_url: 'https://assets.gelbooru.com/images/c0/68/c06832c93190064f5da28ee692811f4f.jpg',
    };
    var query = {
        page: 'dapi',
        s: 'post',
        q: 'index',
        tags: message.query,
        limit: 30
    };
        //tags: 'rating:safe ' + message.query,
    var requestOptions = {
        hostname: 'gelbooru.com',
        port: 443,
        path: '/index.php?' + querystring.stringify(query),
        method: 'GET'
    };
    console.log('/index.php?' + querystring.stringify());
    var apiRequest = https.request(requestOptions, function(res) {
        var data = '';
        res.on('data', function(chunk) {
            data += chunk;
        });
        res.on('end', function() {
            try {
                parseString(data, function (err, result) {
                    var inlineResponses = [];
                    //console.dir(result);
                    //console.log(result.posts.post[0]);
                    result.posts.post.forEach(function(post) {
                        post = post.$;
                        var inlineResponse = {
                            type: 'photo',
                            id: post.md5,
                            photo_url: 'https:' + post.sample_url,
                            thumb_url: 'https:' + post.preview_url
                        };
                        inlineResponses.push(inlineResponse);
                    });
                    console.log('answering now trying');
                    bot.answerInlineQuery(message.id, inlineResponses).then(function(result) {
                        console.log(result);
                    });
                });
            } catch (e) { console.log('we failled ' + e);}
        });
    });
    apiRequest.end();
});


//page=dapi&s=post&q=index&tags=rating%3asafe+nishikino_maki+detached_sleeves

//bot.on('polling_error', function(error) {
//    console.log('we r hear');
//    console.log(error);
//    console.log(error.code);
//    console.log(error.response);
//    console.log(error.response.body);
//});
